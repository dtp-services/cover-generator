const $cover = require('../modules/cover');
const $log = require('../libs/log');
const $request = require('request');

module.exports = function (app) {
  app.get('/get', (req, res) => {
    const title = req.query.title;
    const image = req.query.image;

    if (!title || !image) {
      return res.status(404).end('cover is not found');
    }

    $cover.get({
      title,
      image
    })
      .then(async (url) => {
        $request.get(url).pipe(res);
      })
      .catch((err) => {
        $log.error(err);

        res.status(400).end('Server Error!');
      });
  });
};